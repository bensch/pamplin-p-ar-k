using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Tracking : MonoBehaviour
{
    [SerializeField]
    private string referenceName;

    public ARTrackedImageManager trackedImageManager = null;

    private ARTrackedImage trackedImage;

    void Start()
    {
        StartCoroutine(GetReference());
    }

    private IEnumerator GetReference()
    {
        while (trackedImageManager == null) yield return null;
        foreach (ARTrackedImage image in trackedImageManager.trackables)
        {
            if (image.referenceImage.name == referenceName)
            {
                trackedImage = image;
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = trackedImage.gameObject.transform.position;
    }
}
