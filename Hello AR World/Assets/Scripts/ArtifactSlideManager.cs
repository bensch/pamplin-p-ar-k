using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactSlideManager : MonoBehaviour
{
    [SerializeField]
    private ArtifactSlide[] artifactSlides;

    private int current = -1;

    private GameObject interactable;

    public void Restart()
    {
        StartCoroutine(RestartRoutine());
    }
    private IEnumerator RestartRoutine()
    {
        current = -1;
        for (int i = 0; i < artifactSlides.Length; i++)
        {
            artifactSlides[i].Restart();
            yield return null;
        }
    }
    
    public ArtifactSlide GetCurrentSlide()
    {
        return (current >= 0 && current < artifactSlides.Length)? artifactSlides[current]: null;
    }

    public GameObject GetIntroSlide(int index)
    {
        return artifactSlides[index].GetIntro();
    }

    public GameObject GetInfoSlide(int index)
    {
        return artifactSlides[index].GetInfo();
    }

    public GameObject GetARSlide(int index)
    {
        return artifactSlides[index].GetAR();
    }

    public GameObject GetNextIntroSlide()
    {
        if (++current > 0) artifactSlides[current - 1].GetInfo().SetActive(false);
        if (current < artifactSlides.Length)
        {
            GetIntroSlide(current).SetActive(true);
            return GetIntroSlide(current);
        }
        return null;
    }

    public GameObject GetNextInfoSlide()
    {
        if (current < artifactSlides.Length)
        {
            GetInfoSlide(current).SetActive(true);
            GetCurrentSlide().unfound = false;
            return GetInfoSlide(current);
        }
        return null;
    }

    public GameObject GetLastIntroSlide()
    {
        if (current >= 0 && current < artifactSlides.Length)
        {
            GetInfoSlide(current).SetActive(false);
            GetIntroSlide(current).SetActive(true);
            return GetIntroSlide(current);
        }
        return null;       
    }

    public GameObject GetLastInfoSlide()
    {
        if (--current >= 0 && current < artifactSlides.Length)
        {
            GetInfoSlide(current).SetActive(true);
            GetIntroSlide(current+1).SetActive(false);
            return GetInfoSlide(current);
        }
        return null;
    }

    public GameObject GotoInfoSlide()
    {
        if (current >= 0 && current < artifactSlides.Length)
        {
            Destroy(interactable);
            GetInfoSlide(current).SetActive(true);
            GetARSlide(current).SetActive(false);
            return GetInfoSlide(current);
        }
        return null;
    }

    public GameObject GotoARSlide()
    {
        if (current >= 0 && current < artifactSlides.Length)
        {
            interactable = GetCurrentSlide().GetInteractable();
            GetInfoSlide(current).SetActive(false);
            GetARSlide(current).SetActive(true);
            return GetARSlide(current);
        }
        return null;
    }
}
