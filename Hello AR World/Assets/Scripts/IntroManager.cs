using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{

    [SerializeField]
    private GameObject[] intros;

    [SerializeField]
    private TMPro.TMP_InputField nameInput;

    [SerializeField]
    private TMPro.TMP_Text greetingWithName;

    [SerializeField]
    private int counter = 0;

    [SerializeField]
    private MainController controller;

    [SerializeField]
    private bool skip = false;

    [SerializeField]
    private string username;
    public string GetUsername() => username;

    public void Prime()
    {
        if (skip)
        {
            controller.ExitMenu();
        }
        else
        {
            counter = 0;
            intros[0].SetActive(true);
            for (int i = 1; i < intros.Length; i++)
            {
                intros[i].SetActive(false);
            }
        }
    }

    public void Next()
    {
        if (counter == 1)
        {
            username = nameInput.text;
            greetingWithName.text = string.Format("Welcome to Pamplin P-AR-K, {0}! My name is Mark and I am here to assist you in what you will be doing today here at Pamplin P-AR-K. Here, you will be able to take a journey onto Tudor Hall and go through one of the stories of the enslaved during the Civil War period.\n\nThis application allows you to use AR technology to view artifacts and items that you would not directly touch on the site, allowing you to interact deeply with items from the Civil War era.", username);
        }
        intros[counter++].SetActive(false);
        if (counter < intros.Length)
        {
            intros[counter].SetActive(true);
            Input.backButtonLeavesApp = false;
        }
        else
        {
            controller.NextIntro();
        }
    }

    public void Back()
    {
        if (counter > 0)
        {
            intros[counter--].SetActive(false);
            intros[counter].SetActive(true);
        }
        else
        {
            Input.backButtonLeavesApp = true;
        }
    }
}
