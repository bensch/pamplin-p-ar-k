using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingBody : MonoBehaviour
{
    private bool active = false;

    [SerializeField]
    private GameObject[] groups;

    private int current = 0;

    private Walk walk;
    private void Start()
    {
        walk = this.GetComponent<Walk>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!active && Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                active = hit.transform.Equals(this.transform);
            }
        }
        else if (active && Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 pos = transform.localPosition;
            if (touch.phase == TouchPhase.Moved)
            {
                pos.x += touch.deltaPosition.x;
                walk.destination = pos;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                active = false;
                pos.x = -groups[ClosestGroup()].transform.localPosition.x;
                walk.destination = pos;
            }
        }
    }

    private int ClosestGroup()
    {
        if (current > 0)
        {
            float dist = Mathf.Abs((groups[current - 1].transform.localPosition.x) + this.transform.localPosition.x);
            float required = (Mathf.Abs(groups[current].transform.localPosition.x - groups[current - 1].transform.localPosition.x)) * 3f / 4f;
            Debug.Log(dist + " " + required);
            if (dist <= required) return --current;
        }
        if (current < groups.Length - 1)
        {
            float dist = Mathf.Abs((groups[current + 1].transform.localPosition.x) + this.transform.localPosition.x);
            float required = (Mathf.Abs(groups[current + 1].transform.localPosition.x - groups[current].transform.localPosition.x)) * 3f / 4f;
            Debug.Log(dist + " " + required);
            if (dist <= required) return ++current;
        }
        return current;
    }
}
