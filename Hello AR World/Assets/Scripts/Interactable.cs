using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    [SerializeField]
    private bool rotateY = true;


    private bool active = false;

    // Update is called once per frame
    void Update()
    {
        if (!active && Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 6))
            {
                active = hit.transform.Equals(this.transform);
            }
        }
        else if (active && Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                transform.RotateAround(transform.position, Vector3.up, touch.deltaPosition.x * -0.5f);
                if (rotateY) transform.RotateAround(transform.position, Camera.main.transform.right, touch.deltaPosition.y * 0.5f);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                active = false;
            }
        }
    }
}
