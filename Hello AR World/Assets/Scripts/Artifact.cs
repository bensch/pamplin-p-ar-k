using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact : MonoBehaviour
{
    [Tooltip("Is the artifact unfound."), SerializeField]
    private bool unfound = true;

    [Space, Tooltip("A picture to represent the artifact."), SerializeField]
    private Texture picture;
    public Texture GetPicture() => picture;

    [Tooltip("The title or name of the artifact."), TextArea(1,1), SerializeField]
    private string title;
    public string GetTitle() => title;

    [Space,Tooltip("The story surrounding the artifact."), TextArea(5,10), SerializeField]
    private string story;
    public string GetStory() => story;

    [Space, Tooltip("The info of the artifact."), TextArea(5, 10), SerializeField]
    private string info;
    public string GetInfo() => info;

    [Space, Tooltip("The starting location of the artifact."), SerializeField]
    private Vector3 startingPosition;
    public Vector3 GetStartingPosition() => startingPosition;

    [Tooltip("How many samples to get from the compass before determining the corret angle."), SerializeField]
    private int compassSamples = 10;

    private Walk walk;

    void Start()
    {
        StartCoroutine(PositioningService());
        walk = GetComponent<Walk>();
    }

    private IEnumerator PositioningService()
    {
        while (Input.location.status != LocationServiceStatus.Running)
        {
            yield return new WaitForSeconds(1f);
        }

        // Initial Position
        float angleSum = 0;
        for (int i = 0; i < compassSamples-1; i++)
        {
            angleSum += Input.compass.trueHeading;
            yield return new WaitForSeconds(0.1f);
        }
        angleSum = (angleSum + Input.compass.trueHeading) / compassSamples;
        Vector2 realOrigin = new Vector2(Input.location.lastData.longitude, Input.location.lastData.latitude);
        Vector2 realPos = new Vector2(startingPosition.x, startingPosition.z);
        float realAngle = GetAngle(realOrigin, realPos) - (Mathf.Deg2Rad * angleSum) + (Camera.main.transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
        float realDist = Vector2.Distance(realOrigin, realPos) * 100000;
        Vector3 pos = Vector3.zero;
        pos.x = (Mathf.Sin(realAngle) * realDist) + Camera.main.transform.position.x;
        pos.y = startingPosition.y;
        pos.z = (Mathf.Cos(realAngle) * realDist) + Camera.main.transform.position.z;
        transform.position = pos;
        angleSum = 0;

        // Update Position
        for (; ; )
        {
            for (int i = 0; i < compassSamples - 1; i++)
            {
                angleSum += Input.compass.trueHeading;
                yield return new WaitForSeconds(0.1f);
            }
            angleSum = (angleSum + Input.compass.trueHeading) / compassSamples;
            realOrigin = new Vector2(Input.location.lastData.longitude, Input.location.lastData.latitude);
            realPos = new Vector2(startingPosition.x, startingPosition.z);
            realAngle = GetAngle(realOrigin, realPos) - (Mathf.Deg2Rad * angleSum) + (Camera.main.transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
            realDist = Vector2.Distance(realOrigin, realPos) * 100000;
            pos = Vector3.zero;
            pos.x = (Mathf.Sin(realAngle) * realDist) + Camera.main.transform.position.x;
            pos.y = startingPosition.y;
            pos.z = (Mathf.Cos(realAngle) * realDist) + Camera.main.transform.position.z;
            walk.destination = pos;
            angleSum = 0;
        }
    }

    public bool IsUnfound()
    {
        return unfound;
    }

    public void Found()
    {
        unfound = false;
    }

    private float GetAngle(Vector2 origin, Vector2 target)
    {
        float ret = Mathf.Atan((target.x - origin.x) / (target.y - origin.y));
        if (target.y < origin.y)
        {
            return ret + Mathf.PI;
        }
        return (target.x < origin.x) ? ret + (Mathf.PI*2) : ret;
    }
}