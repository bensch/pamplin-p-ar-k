using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBoxController : MonoBehaviour
{
    [Tooltip("The Main Controller of the project."), SerializeField]
    private MainController interactionManager;

    [Tooltip("The content of the info box."), SerializeField]
    private RectTransform content;

    [Tooltip("The scroll bar."), SerializeField]
    private Scrollbar scrollbar;

    [SerializeField]
    private RawImage picture;

    [SerializeField]
    private Text title;

    [SerializeField]
    private Text description;

    [SerializeField]
    private Button storyButton;

    [SerializeField]
    private Button infoButton;

    [SerializeField]
    private Artifact artifact;

    private void Start()
    {
        Enable(artifact);
    }

    public void OnDisable()
    {
        ResetScrollBar();
    }

    public void Enable(Artifact artifact)
    {
        this.artifact = artifact;
        this.picture.texture = artifact.GetPicture();
        this.title.text = artifact.GetTitle();
        OnStory();
        this.gameObject.SetActive(true);
    }

    public void OnStory()
    {
        ResetScrollBar();
        this.description.text = artifact.GetStory();
        this.storyButton.interactable = false;
        this.infoButton.interactable = true;
    }

    public void OnInfo()
    {
        ResetScrollBar();
        this.description.text = artifact.GetInfo();
        this.storyButton.interactable = true;
        this.infoButton.interactable = false;
    }

    private void ResetScrollBar()
    {
        content.offsetMax = new Vector2(-4.9f, 0.0f);
        content.offsetMin = new Vector2(-2.0f, -325.0f);
        scrollbar.value = 1f;
    }
}
