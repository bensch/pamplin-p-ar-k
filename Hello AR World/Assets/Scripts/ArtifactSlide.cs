using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ArtifactSlide : MonoBehaviour
{

    [SerializeField]
    private GameObject intro;
    public GameObject GetIntro() => intro;

    [SerializeField]
    private GameObject info;
    public GameObject GetInfo() => info;

    [SerializeField]
    private GameObject AR;
    public GameObject GetAR() => AR;

    [SerializeField]
    private float interactableSpawnDistance = 1f;

    [SerializeField]
    private GameObject interactable;
    public GameObject GetInteractable()
    {
        foreach (ARTrackedImage trackedImage in m_TrackedImageManager.trackables)
        {
            if (HasReferenceName(trackedImage.referenceImage.name))
            {
                GameObject ret = Instantiate(interactable, trackedImage.transform.position, Quaternion.identity);
                ret.GetComponent<Tracking>().trackedImageManager = m_TrackedImageManager;
                return ret;
            }
        }
        return Instantiate(interactable, Camera.main.transform.position + (Camera.main.transform.forward * interactableSpawnDistance), Quaternion.identity);
    }

    [SerializeField]
    public bool unfound = true;

    [SerializeField]
    ARTrackedImageManager m_TrackedImageManager;

    [SerializeField]
    private string[] referenceNames;

    public bool HasReferenceName(string refName)
    {
        foreach (string n in referenceNames)
        {
            if (n.Equals(refName)) return true;
        }
        return false;
    }

    public void Restart()
    {
        intro.SetActive(false);
        info.SetActive(false);
        unfound = true;
    }
}
