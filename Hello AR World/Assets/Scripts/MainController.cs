using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{

    [Tooltip("The maximum distance in meters a user can interact with artifacts."), SerializeField]
    private float maxInteractDist = 1f;

    [Tooltip("The info box to update."), SerializeField]
    private InfoBoxController infoBox;

    [Tooltip("The Greeting Card."), SerializeField]
    private IntroManager greetingCard;

    [Tooltip("The Artifact Manager."), SerializeField]
    private ArtifactManager artifactManager;

    [Tooltip("The Guide."), SerializeField]
    private GameObject guide;

    [SerializeField]
    private bool inMenu = true;

    [SerializeField]
    private ArtifactSlideManager artifactSlideManager;

    [SerializeField]
    private GameObject finale;
    [SerializeField]
    private TMPro.TMP_Text finaleName;

    [SerializeField]
    private GameObject search;

    public Artifact testArtifact = null;
    public bool debug = false;
    public Text imageName;

    public bool skipSearch = false;

    private GameObject currentSlide;
    public void NextIntro()
    {
        if ((currentSlide = artifactSlideManager.GetNextIntroSlide()) == null)
        {
            finaleName.text = greetingCard.GetUsername().ToUpper();
            finale.SetActive(true);
        }
        m_TrackedImageManager.enabled = false;
    }

    public void NextInfo()
    {
        inMenu = true;
        currentSlide = artifactSlideManager.GetNextInfoSlide();
        search.SetActive(false);
    }

    public void BackIntro()
    {
        currentSlide = artifactSlideManager.GetLastIntroSlide();
    }

    public void BackInfo()
    {
        currentSlide = artifactSlideManager.GetLastInfoSlide();
    }

    public void ToAR()
    {
        currentSlide = artifactSlideManager.GotoARSlide();
    }

    public void ToInfo()
    {
        currentSlide = artifactSlideManager.GotoInfoSlide();
    }

    public void CloseCurrent()
    {
        currentSlide.SetActive(false);
        if (artifactSlideManager.GetCurrentSlide().unfound)
        {
            inMenu = false;
            m_TrackedImageManager.enabled = true;
            search.SetActive(true);
        }
        else
        {
            NextInfo();
        }
    }

    private bool needsToLoad = false;
    public void ExitMenu()
    {
        infoBox.gameObject.SetActive(false);
        greetingCard.gameObject.SetActive(false);
        if (debug) infoBox.Enable(testArtifact);
        else inMenu = false;
        if (needsToLoad)
        {
            needsToLoad = false;
            artifactManager.InstantiateArtifacts();
            guide.SetActive(true);
        }
    }

    [SerializeField]
    ARTrackedImageManager m_TrackedImageManager;

    void OnEnable() => m_TrackedImageManager.trackedImagesChanged += OnChanged;

    void OnDisable() => m_TrackedImageManager.trackedImagesChanged -= OnChanged;

    void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        if (!inMenu)
        {
            foreach (var newImage in eventArgs.added)
            {
                imageName.text = newImage.referenceImage.name;
                if (artifactSlideManager.GetCurrentSlide().HasReferenceName(newImage.referenceImage.name))
                {
                    NextInfo();
                }
            }

            foreach (var newImage in eventArgs.updated)
            {
                imageName.text = newImage.referenceImage.name;
                if (artifactSlideManager.GetCurrentSlide().HasReferenceName(newImage.referenceImage.name))
                {
                    NextInfo();
                }
            }
        }
    }

    void Start()
    {
        if (debug) imageName.gameObject.SetActive(true);
        if (!skipSearch) artifactSlideManager.Restart();
        m_TrackedImageManager.enabled = false;
        greetingCard.gameObject.SetActive(true);
        greetingCard.Prime();
    }

    // Update is called once per frame
    void Update()
    {
        // Early exit
        if (Input.touchCount == 0)
        {
            return;
        }

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); 

        if (!inMenu && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (Physics.Raycast(ray, out hit, maxInteractDist))
            {
                if (hit.collider.gameObject.tag == "Artifact")
                {
                    Artifact artifact = hit.collider.gameObject.GetComponent<Artifact>();
                    artifact.Found();
                    infoBox.Enable(artifact);
                    inMenu = true;
                }
            }
        }
    }
}
