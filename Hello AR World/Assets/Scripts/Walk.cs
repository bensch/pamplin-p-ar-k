using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : MonoBehaviour
{
    [SerializeField]
    private float walkPace = 1f;

    [SerializeField]
    private bool useLocalPosition = false;

    [SerializeField]
    public Vector3 destination;

    // Update is called once per frame
    void Update()
    {
        if (useLocalPosition) transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination, walkPace * Time.deltaTime);
        else transform.position = Vector3.MoveTowards(transform.position, destination, walkPace * Time.deltaTime);
    }
}
