using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour
{

    [Tooltip("The artifact manager the guide gets artifacts from."), SerializeField]
    private ArtifactManager artifactManager;

    [Tooltip("How far the guide should be from the camera."), SerializeField]
    private float distanceFromCamera = 1f;

    [Tooltip("The y offset from the camera."), SerializeField]
    private float yOffset = -1f;

    [Tooltip("The material of the guide."), SerializeField]
    private Material material;

    private Camera cam;
    private Walk walk;

    void Start()
    {
        cam = Camera.main;
        walk = GetComponent<Walk>();
    }

    // Update is called once per frame
    void Update()
    {
        Artifact artifact = artifactManager.GetNextArtifact();

        // Do compass stuff
        if (artifact != null)
        {
            Vector3 pos = Vector3.MoveTowards(cam.transform.position, artifact.transform.position, distanceFromCamera);
            pos.y = cam.transform.position.y + yOffset;
            walk.destination = pos;
            material.color = new Color(1f,1f,1f,1f);
        }
        // Done
        else
        {
            material.color = new Color(1f, 1f, 1f, 0f);
        }
    }
}
