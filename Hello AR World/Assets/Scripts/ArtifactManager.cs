using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

[CreateAssetMenu]
public class ArtifactManager : ScriptableObject
{
    // List of prefabs
    [SerializeField]
    private Artifact[] artifacts;

    // Objects in the actual scene
    private List<Artifact> newArtifacts = null;

    public void InstantiateArtifacts()
    {
        newArtifacts = new List<Artifact>();
        foreach (Artifact artifact in artifacts)
        {
            newArtifacts.Add(Instantiate(artifact, artifact.GetStartingPosition(), Quaternion.identity));
        }
    }

    public Artifact GetNextArtifact()
    {
        foreach (Artifact current in newArtifacts)
        {
            if (current == null) return null;
            else if (current.IsUnfound()) return current;
        }
        return null;
    }
}
