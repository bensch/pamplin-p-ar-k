using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class GPS : MonoBehaviour
{
    public float latitude;
    public float longitude;
    public float altitude;
    public bool debug = true;

    [SerializeField]
    ARTrackedImageManager artim;
    [SerializeField]
    GameObject debugParent;
    [SerializeField]
    Text lat;
    [SerializeField]
    Text lon;
    [SerializeField]
    Text alt;
    [SerializeField]
    Text comp;
    [SerializeField]
    Text tracked;

    // Start is called before the first frame update
    void Start()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);
        }

        StartCoroutine(LocationService());
    }

    private IEnumerator LocationService()
    {
        // Check whether location service is eneabled
        if (Input.location.isEnabledByUser)
        {
            Input.location.Start(0.3f, 1f);
        }

        // Wait for the service to initialize
        while (Input.location.status == LocationServiceStatus.Initializing)
        {
            yield return new WaitForSeconds(1);
        }

        // Make sure the service started correctly
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            yield break;
        }
        Input.compass.enabled = true;


        // Display Cordinates for debuging and development
        if (debug)
        {
            debugParent.SetActive(true);
            for (;;)
            {
                latitude = Input.location.lastData.latitude;
                lat.text = "" + latitude;
                longitude = Input.location.lastData.longitude;
                lon.text = "" + longitude;
                altitude = Input.location.lastData.altitude;
                alt.text = "" + altitude;
                comp.text = "" + Input.compass.trueHeading;
                tracked.text = "" + artim.trackables.count;

                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
